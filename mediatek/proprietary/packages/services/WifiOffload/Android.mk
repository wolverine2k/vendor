LOCAL_PATH:= $(call my-dir)

# interface lib
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, lib/src)

LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)/lib/src

# aidl
LOCAL_SRC_FILES += \
    lib/src/com/mediatek/wfo/IWifiOffloadService.aidl \
    lib/src/com/mediatek/wfo/IWifiOffloadListener.aidl

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := wfo-common

include $(BUILD_STATIC_JAVA_LIBRARY)

# JNI
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    jni/com_mediatek_wfo_WifiOffloadService.c

# Res
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res

LOCAL_MULTILIB := both
LOCAL_ARM_MODE := arm

LOCAL_SHARED_LIBRARIES := \
    libnativehelper \
    libcutils \
    liblog \
    libutils \
    libmal \
    libmdfx

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../frameworks/opt/mal/libmdfx/include/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../frameworks/opt/mal/interface/include/

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libwfo_jni

include $(BUILD_SHARED_LIBRARY)

# apk
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_JAVA_LIBRARIES += telephony-common ims-common

# Add for Plug-in, include the plug-in framework
LOCAL_JAVA_LIBRARIES += mediatek-framework

LOCAL_STATIC_JAVA_LIBRARIES += wfo-common

LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := WfoService
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_PROGUARD_ENABLED := disabled
LOCAL_PROGUARD_FLAGS := $(proguard.flags)

include $(BUILD_PACKAGE)
