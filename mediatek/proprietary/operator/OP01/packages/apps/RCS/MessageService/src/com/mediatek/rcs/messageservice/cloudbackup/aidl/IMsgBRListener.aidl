package com.mediatek.rcs.messageservice.cloudbackup.aidl;

interface IMsgBRListener {
    void onBackupResult(in int result, in String msgPath, in String favPath);
    void onRestoreResult(in int result);
}
