
#include "FakeSensor.h"

#define SENSOR_PREVIEW_MODE     (0)
#define SENSOR_CAPTURE_MODE     (1)
#define SENSOR_MODE             (1)

#define SEOSOR_PIXEL_MODE       (1)

//4176; 2088; 1600;
//3088; 1544; 1200;
#if (SENSOR_MODE == SENSOR_PREVIEW_MODE)
#define SENSOR_WIDTH        (1600)
#define SENSOR_HEIGHT       (1200)
#define SENSOR_WIDTH_SUB    (1600)
#define SENSOR_HEIGHT_SUB   (1200)
#elif (SENSOR_MODE == SENSOR_CAPTURE_MODE)
#define SENSOR_WIDTH        (1600)//3200 4176
#define SENSOR_HEIGHT       (1200)//2400 3088
#define SENSOR_WIDTH_SUB    (1600)
#define SENSOR_HEIGHT_SUB   (1200)
#else
#define SENSOR_WIDTH        (1600)
#define SENSOR_HEIGHT       (1200)
#define SENSOR_WIDTH_SUB    (1600)
#define SENSOR_HEIGHT_SUB   (1200)
#endif

SensorStaticInfo TS_FakeSensorList::mSInfo;
IMetadata TS_FakeSensorList::mDummyMetadata;

IHalSensorList *TS_FakeSensorList::getTestModel()
{
    static TS_FakeSensorList single;
    return &single;
}

IHalSensor *TS_FakeSensorList::createSensor (
                char const* szCallerName,
                MUINT const uCountOfIndex,
                MUINT const*pArrayOfIndex)
{
    static TS_FakeSensor single[2];

    TS_LOGD("create : %d", pArrayOfIndex[0]);
    if (pArrayOfIndex[0] >= 2) {
        TS_LOGE("Wrong sensor idx: %d", pArrayOfIndex[0]);
        return NULL;
    }

    (void)szCallerName;
    (void)uCountOfIndex;

    return &single[pArrayOfIndex[0]];
}

MVOID TS_FakeSensorList::querySensorStaticInfo (
        MUINT sensorDevIdx,
        SensorStaticInfo *pSensorStaticInfo) const
{
    (void)sensorDevIdx;

    TS_LOGD("+");

    memset((void*)pSensorStaticInfo, 0, sizeof(SensorStaticInfo));

    if (sensorDevIdx & 0x01) {
        TS_LOGD("dev 0x1");
        pSensorStaticInfo[0].sensorDevID = 0x5566;
        pSensorStaticInfo[0].sensorType = SENSOR_TYPE_RAW;
        pSensorStaticInfo[0].sensorFormatOrder = 1;
        pSensorStaticInfo[0].rawSensorBit = RAW_SENSOR_10BIT;

        pSensorStaticInfo[0].previewWidth = SENSOR_WIDTH;
        pSensorStaticInfo[0].previewHeight = SENSOR_HEIGHT;
        pSensorStaticInfo[0].captureWidth = SENSOR_WIDTH;
        pSensorStaticInfo[0].captureHeight = SENSOR_HEIGHT;
        pSensorStaticInfo[0].videoWidth = SENSOR_WIDTH;
        pSensorStaticInfo[0].videoWidth = SENSOR_HEIGHT;

        pSensorStaticInfo[0].orientationAngle = 90;

        pSensorStaticInfo[0].previewDelayFrame = 0;
        pSensorStaticInfo[0].captureDelayFrame = 0;
        pSensorStaticInfo[0].videoDelayFrame = 0;

        pSensorStaticInfo[0].previewFrameRate = 300;
        pSensorStaticInfo[0].captureFrameRate = 150;
        pSensorStaticInfo[0].videoFrameRate = 300;
    }
    if (sensorDevIdx & 0x02) {
        TS_LOGD("dev 0x2");
        pSensorStaticInfo[0].sensorDevID = 0x5566;
        pSensorStaticInfo[0].sensorType = SENSOR_TYPE_RAW;
        pSensorStaticInfo[0].sensorFormatOrder = 1;
        pSensorStaticInfo[0].rawSensorBit = RAW_SENSOR_10BIT;

        pSensorStaticInfo[0].previewWidth = SENSOR_WIDTH_SUB;
        pSensorStaticInfo[0].previewHeight = SENSOR_HEIGHT_SUB;
        pSensorStaticInfo[0].captureWidth = SENSOR_WIDTH_SUB;
        pSensorStaticInfo[0].captureHeight = SENSOR_HEIGHT_SUB;
        pSensorStaticInfo[0].videoWidth = SENSOR_WIDTH_SUB;
        pSensorStaticInfo[0].videoWidth = SENSOR_HEIGHT_SUB;

        pSensorStaticInfo[0].orientationAngle = 90;

        pSensorStaticInfo[0].previewDelayFrame = 0;
        pSensorStaticInfo[0].captureDelayFrame = 0;
        pSensorStaticInfo[0].videoDelayFrame = 0;

        pSensorStaticInfo[0].previewFrameRate = 300;
        pSensorStaticInfo[0].captureFrameRate = 150;
        pSensorStaticInfo[0].videoFrameRate = 300;
    }
}


static MUINT32 mPowerOnTM[] = {
        0x15040000, 0x00000C00, //SENINF_TOP_CTRL
        0x15040200, 0x00001001,
        0x15040d00, 0x96DF1080,
        0x15040d04, 0x8000007F,
        0x15040d0c, 0x00000000,
        0x15040d2c, 0x000E2000,
        0x15040d38, 0x00000000,
        0x15040608, 0x000404C1, // TG1_TM_CTL
                                // [7:4]: green(3), red(5), blue(6), H_BAR(12), V_BAR(13)
        0x1504060c, 0x0FA01F00, // TG1_TM_SIZE
        0x15040610, 0x00000000, // TG1_TM_CLK
        0x15040614, 0x1        // always setting timestamp dividor to 1 for test only
};

MBOOL TS_FakeSensor::powerOn(
        char const* szCallerName,
        MUINT const uCountOfIndex,
        MUINT const*pArrayOfIndex)
{
    MINT32 nNum = 0, i = 0, ret = 0;
    (void)szCallerName; (void)uCountOfIndex; (void)pArrayOfIndex;

    TS_LOGD("PowerOn: %d", pArrayOfIndex[0]);

    m_pIspDrv = (IspDrvImp*)IspDrvImp::createInstance((pArrayOfIndex[0] == 0)? CAM_A: CAM_B);
    if (NULL == m_pIspDrv) {
        TS_LOGD("Error: IspDrv CreateInstace fail");
        return 0;
    }

    ret = m_pIspDrv->init("FakeSensor");
    if (ret < 0) {
        TS_LOGD("Error: IspDrv init fail");
        return 0;
    }
    m_pIspDrv->setRWMode(ISP_DRV_RW_IOCTL);//specail control for start seninf , for test code only
    TS_LOGD("Set TM enable");
    m_pIspDrv->writeReg(0x15040204 - 0x15040000 , m_pIspDrv->readReg(0x15040204 - 0x15040000 ) | 0x02);

    nNum = (sizeof(mPowerOnTM) / sizeof(mPowerOnTM[0])) / 2;
    TS_LOGD("Total %d registers", nNum);

    for (i = 0; i < nNum; i++) {
        #if 0 //change test model pattern
        if (0x15040608 == mPowerOnTM[i<<1]) {
            TS_LOGD("Seninf Test Mode : %d", (atoi(szCallerName) & 0xF));
            mPowerOnTM[(i<<1)+1] = (0x00040401 | ((atoi(szCallerName) & 0xF) << 4));
        }
        #endif
        m_pIspDrv->writeReg(mPowerOnTM[i<<1] - 0x15040000, mPowerOnTM[(i<<1)+1]);
    }
    #if (SEOSOR_PIXEL_MODE == 2) //two pixel mode
    TS_LOGD("Set two pixel mode");
    m_pIspDrv->writeReg(0x15040D00 - 0x15040000 , m_pIspDrv->readReg(0x15040D00 - 0x15040000 ) | 0x100);
    m_pIspDrv->writeReg(0x15040D3C - 0x15040000 , m_pIspDrv->readReg(0x15040D3C - 0x15040000 ) & 0x10);
    #elif (SEOSOR_PIXEL_MODE == 4)//four pixel mode
    TS_LOGD("Set four pixel mode");
    m_pIspDrv->writeReg(0x15040D00 - 0x15040000 , m_pIspDrv->readReg(0x15040D00 - 0x15040000 ) & ~0x100);
    m_pIspDrv->writeReg(0x15040D3C - 0x15040000 , m_pIspDrv->readReg(0x15040D3C - 0x15040000 ) | 0x10);
    #else
    TS_LOGD("Set one pixel mode");
    m_pIspDrv->writeReg(0x15040D00 - 0x15040000 , m_pIspDrv->readReg(0x15040D00 - 0x15040000 ) & ~0x100);
    m_pIspDrv->writeReg(0x15040D3C - 0x15040000 , m_pIspDrv->readReg(0x15040D3C - 0x15040000 ) & ~0x10);
    #endif
    if (0) //(1 == bYUV)
    {
        m_pIspDrv->writeReg(0x0608, m_pIspDrv->readReg(0x0608) | 0x4);
    }

    for (i = 0; i < nNum; i++) {
        TS_LOGD(" Reg[x%08x] = x%08x/x%08x", mPowerOnTM[i<<1],
            m_pIspDrv->readReg(mPowerOnTM[i<<1] - 0x15040000), mPowerOnTM[(i<<1)+1]);
    }

    m_pIspDrv->setRWMode(ISP_DRV_RW_MMAP);

    return 1;
}

MBOOL TS_FakeSensor::powerOff(
        char const* szCallerName,
        MUINT const uCountOfIndex,
        MUINT const*pArrayOfIndex)
{
    (void)szCallerName; (void)uCountOfIndex; (void)pArrayOfIndex;
    if (m_pIspDrv) {
        TS_LOGD("Set TM disable");
        m_pIspDrv->writeReg(0x15040204 - 0x15040000 , m_pIspDrv->readReg(0x15040204 - 0x15040000 ) & ~0x02);
        m_pIspDrv->uninit("FakeSensor");
        m_pIspDrv->destroyInstance();
        m_pIspDrv = NULL;
    }

    return 1;
}

MBOOL TS_FakeSensor::configure(
        MUINT const uCountOfParam,
        IHalSensor::ConfigParam const*  pArrayOfParam)
{
    (void)uCountOfParam; (void)pArrayOfParam;
    return 1;
}

MBOOL TS_FakeSensor::querySensorDynamicInfo(
          MUINT32 sensorIdx,
          SensorDynamicInfo *pSensorDynamicInfo)
{
    TS_LOGD("sensorIdx: x%x", sensorIdx);

    if (sensorIdx == 0x01) {
        pSensorDynamicInfo->TgInfo = CAM_TG_1;
        #if (SEOSOR_PIXEL_MODE == 2)
        pSensorDynamicInfo->pixelMode = TWO_PIXEL_MODE;
        #elif (SEOSOR_PIXEL_MODE == 4)
        pSensorDynamicInfo->pixelMode = FOUR_PIXEL_MODE;
        #else
        pSensorDynamicInfo->pixelMode = ONE_PIXEL_MODE;
        #endif
    }
    if (sensorIdx == 0x02) {
        pSensorDynamicInfo->TgInfo = CAM_TG_2;
        #if (SEOSOR_PIXEL_MODE == 2)
        pSensorDynamicInfo->pixelMode = TWO_PIXEL_MODE;
        #elif (SEOSOR_PIXEL_MODE == 4)
        pSensorDynamicInfo->pixelMode = FOUR_PIXEL_MODE;
        #else
        pSensorDynamicInfo->pixelMode = ONE_PIXEL_MODE;
        #endif
    }

    return 1;
}
