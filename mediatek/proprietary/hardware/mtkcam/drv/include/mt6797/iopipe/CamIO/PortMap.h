/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_PLATFORM_HARDWARE_MTKCAM_CORE_IOPIPE_CAMIO_PORTMAP_H_
#define _MTK_PLATFORM_HARDWARE_MTKCAM_CORE_IOPIPE_CAMIO_PORTMAP_H_
//
#include <Port.h>


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace NSIoPipe {
//
  static const PortID       PORT_IMGO  (EPortType_Memory, 6/*NSImageio::NSIspio::EPortIndex_IMGO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_RRZO  (EPortType_Memory, 8/*NSImageio::NSIspio::EPortIndex_RRZO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_CAMSV_IMGO (EPortType_Memory, 9/*NSImageio::NSIspio::EPortIndex_CAMSV_IMGO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_CAMSV2_IMGO (EPortType_Memory, 10/*NSImageio::NSIspio::EPortIndex_CAMSV2_IMGO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);

  static const PortID       PORT_LCSO  (EPortType_Memory, 11/*NSImageio::NSIspio::EPortIndex_LCSO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_AAO  (EPortType_Memory, 12/*NSImageio::NSIspio::EPortIndex_AAO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_AFO  (EPortType_Memory, 13/*NSImageio::NSIspio::EPortIndex_AFO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_PDO  (EPortType_Memory, 14/*NSImageio::NSIspio::EPortIndex_PDO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_EISO  (EPortType_Memory, 15/*NSImageio::NSIspio::EPortIndex_EISO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  static const PortID       PORT_FLKO  (EPortType_Memory, 16/*NSImageio::NSIspio::EPortIndex_FLKO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);
  //static const PortID       PORT_RSSO  (EPortType_Memory, 17/*NSImageio::NSIspio::EPortIndex_RSSO*//*index*/, 1/*in/out*/,EPortCapbility_None/*capbility*/,0/*frame group*/);

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace NSIoPipe
};  //namespace NSCam
#endif  //_MTK_PLATFORM_HARDWARE_MTKCAM_CORE_IOPIPE_POSTPROC_PORTMAP_H_

