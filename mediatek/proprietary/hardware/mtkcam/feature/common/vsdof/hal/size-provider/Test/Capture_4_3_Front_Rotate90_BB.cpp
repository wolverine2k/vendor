#include "StereoSizeProviderUT.h"

#define MY_LOGD(fmt, arg...)    printf("[D][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGI(fmt, arg...)    printf("[I][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGW(fmt, arg...)    printf("[W][%s] WRN(%5d):" fmt"\n", __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    printf("[E][%s] %s ERROR(%5d):" fmt"\n", __func__,__FILE__, __LINE__, ##arg)

#define UT_TAG "[Capture_4_3_Front_Rotate90_BB]"

class Capture_4_3_Front_Rotate90_BB: public ::testing::Test
{
public:
    Capture_4_3_Front_Rotate90_BB()
        : SENSOR_COUNT(NSCam::IHalSensorList::get()->queryNumberOfSensors())
    {
        scenario                               = eSTEREO_SCENARIO_CAPTURE;
        StereoSettingProvider::imageRatio()    = eRatio_4_3;
        StereoSettingProvider::stereoProfile() = STEREO_SENSOR_PROFILE_FRONT_FRONT;
        StereoSettingProvider::isDeNoise()     = false;
        //Module rotation is defined in camera_custom_stereo.cpp of each project in custom folder

        sizeProvider = StereoSizeProvider::getInstance();
    }

    virtual ~Capture_4_3_Front_Rotate90_BB() {}

protected:
    virtual void SetUp() {
        if(SENSOR_COUNT < 4) {
            return;
        }

        sizeProvider->getPass1Size( eSTEREO_SENSOR_MAIN1,
                                    eImgFmt_FG_BAYER10,
                                    EPortIndex_RRZO,
                                    scenario,
                                    //below are outputs
                                    tgCropRect[0],
                                    szRRZO[0],
                                    junkStride);

        sizeProvider->getPass1Size( eSTEREO_SENSOR_MAIN2,
                                    eImgFmt_FG_BAYER10,
                                    EPortIndex_RRZO,
                                    scenario,
                                    //below are outputs
                                    tgCropRect[1],
                                    szRRZO[1],
                                    junkStride);

        sizeProvider->getPass1Size( eSTEREO_SENSOR_MAIN1,
                                    eImgFmt_BAYER10,
                                    EPortIndex_IMGO,
                                    scenario,
                                    //below are outputs
                                    tgCropRect[2],
                                    szMainIMGO,
                                    junkStride);

        //=============================================================================
        //  PASS 2
        //=============================================================================
        sizeProvider->captureImageSize() = MSize(3072, 2304);
        sizeProvider->getPass2SizeInfo(PASS2A,      scenario,   pass2SizeInfo[PASS2A]);
        sizeProvider->getPass2SizeInfo(PASS2A_P,    scenario,   pass2SizeInfo[PASS2A_P]);
        sizeProvider->getPass2SizeInfo(PASS2A_2,    scenario,   pass2SizeInfo[PASS2A_2]);
        sizeProvider->getPass2SizeInfo(PASS2A_P_2,  scenario,   pass2SizeInfo[PASS2A_P_2]);
        sizeProvider->getPass2SizeInfo(PASS2A_3,    scenario,   pass2SizeInfo[PASS2A_3]);
        sizeProvider->getPass2SizeInfo(PASS2A_P_3,  scenario,   pass2SizeInfo[PASS2A_P_3]);
    }

    // virtual void TearDown() {}

protected:
    //Init
    const int SENSOR_COUNT;
    StereoSizeProvider *sizeProvider;
    ENUM_STEREO_SCENARIO scenario;

    //Pass 1
    MUINT32 junkStride;
    MRect tgCropRect[3];
    MSize szRRZO[2];
    MRect activityArray[2];
    MSize szMainIMGO;

    //Pass 2
    Pass2SizeInfo pass2SizeInfo[6];
};

//=============================================================================
//  PASS 1
//=============================================================================
TEST_F(Capture_4_3_Front_Rotate90_BB, TEST)
{
    ASSERT_GE(SENSOR_COUNT, 4);

    //=============================================================================
    //  Pass 1
    //=============================================================================
    MYEXPECT_EQ( tgCropRect[0],     MRect(MPoint(0, 0),     MSize(4208, 3120)) );
    MYEXPECT_EQ( szRRZO[0],                                 MSize(2100, 1560)  );
    MYEXPECT_EQ( tgCropRect[1],     MRect(MPoint(0, 0),     MSize(2592, 1944)) );
    MYEXPECT_EQ( szRRZO[1],                                 MSize(1296, 972)   );
    MYEXPECT_EQ( tgCropRect[2],     MRect(MPoint(0, 0),     MSize(4208, 3120)) );
    MYEXPECT_EQ( szMainIMGO,                                MSize(4208, 3120) );
    MYEXPECT_EQ( StereoSizeProvider::getInstance()->getSBSImageSize(), MSize(3392, 1984));

    //=============================================================================
    //  Pass 2
    //=============================================================================
    //Pass2-A
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].areaWDMA.size,        MSize(3072, 2304));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].szWROT,               MSize(720, 960));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].areaFEO.size,         MSize(1600, 1200));

    // PASS2A'
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P].szWROT,             MSize(720, 960));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P].areaFEO.size,       MSize(1600, 1200));

    // PASS2A-2
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].areaWDMA,           StereoArea(1696, 1984, 256, 64, 128, 32));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].szIMG2O,            MSize(368, 480));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].areaFEO.size,       MSize(720, 960));

    // PASS2A'-2
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].areaWDMA,         StereoArea(1696, 1984, 256, 64, 128, 32));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].szIMG2O,          MSize(368, 480));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].areaFEO,          StereoArea(MSize(720, 960), MSize(0, 0), MPoint(0, 0)));

    // PASS2A-3
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_3].szIMG2O,            MSize(96, 128));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_3].areaFEO.size,       MSize(368, 480));

    // PASS2A'-3
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_3].szIMG2O,          MSize(96, 128));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_3].areaFEO,          StereoArea(MSize(368, 480), MSize(0, 0), MPoint(0, 0)));

    // PASS2B
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_WROT, scenario), StereoArea(3072, 2304));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_WDMA, scenario), StereoArea(3072, 2304));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_3DNR, scenario), STEREO_AREA_ZERO);

    //=============================================================================
    //  Buffers
    //=============================================================================
    //N3D Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MV_Y),            StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_M_Y),        StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_SV_Y),            StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_S_Y),        StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_LDC),             StereoArea(212, 248, 32, 8, 16, 4));

    //N3D before MDP for capture
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MV_Y_LARGE),      StereoArea(1696, 1984, 256, 64, 128, 32));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_M_Y_LARGE),  StereoArea(1696, 1984, 256, 64, 128, 32));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_SV_Y_LARGE),      StereoArea(1696, 1984, 256, 64, 128, 32));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_S_Y_LARGE),  StereoArea(1696, 1984, 256, 64, 128, 32));

    //DPE Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMP_H),           StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_CFM_H),           StereoArea(212, 248, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_RESPO),           StereoArea(212, 248, 32, 8, 16, 4));

    //OCC Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MY_S),            StereoArea(180, 240));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMH),             StereoArea(180, 240));

    //WMF Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMW),             StereoArea(180, 240));

    //GF Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMG),             StereoArea(240, 180));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMBG),            StereoArea(240, 180));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DEPTH_MAP),       StereoArea(480, 368));
}
