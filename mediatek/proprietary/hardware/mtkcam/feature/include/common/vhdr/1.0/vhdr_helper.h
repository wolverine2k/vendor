/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

/**
 * @file vhdr_helper.h
 *
 * VHDR Helper File
 *
 */

#ifndef _VHDR_HELPER_H_
#define _VHDR_HELPER_H_

#include "IHalSensor.h"
#include "isp_tuning.h" // For EIspProfie_T

class VHdrHelper
{
    public:

        /**
         *@brief VHdrHelper constructor
         */
        VHdrHelper() {};

        /**
         *@brief In VHDR Mode, you can use this function to know video size needs scale down or not.
         *@param[in] vhdrMode : mode in IParamsManager.getVHdr()
         */
        static MINT32 getVideoSizeDivision(const MUINT32 vhdrMode){
            if(vhdrMode == SENSOR_VHDR_MODE_IVHDR)
                return 2;
            else
                return 1;
        }

        /**
         *@brief In flow control (preview, record), you can use this function to get vhdr 3A profile
         * If not vhdr mode, it will return Normal Preview profile.
         *@param[in] isRecording : you can use IParamsManager.getRecordingHint()
         *@param[in] vhdrMode : mode in IParamsManager.getVHdr()
         */
        static NSIspTuning::EIspProfile_T getFlowControlVHDR3AProfile(bool isRecording, MUINT32 vhdrMode)
        {
            if(vhdrMode == SENSOR_VHDR_MODE_NONE)
                return NSIspTuning::EIspProfile_Preview;

            if(isRecording)
            {
                switch(vhdrMode)
                {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        return NSIspTuning::EIspProfile_zHDR_Video;
                    case SENSOR_VHDR_MODE_MVHDR:
                        return NSIspTuning::EIspProfile_MHDR_Video;
                    case SENSOR_VHDR_MODE_IVHDR:
                        return NSIspTuning::EIspProfile_IHDR_Video;
                }
            }
            else
            { // Preview
                switch(vhdrMode)
                {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        return NSIspTuning::EIspProfile_zHDR_Preview;
                    case SENSOR_VHDR_MODE_MVHDR:
                        return NSIspTuning::EIspProfile_MHDR_Preview;
                    case SENSOR_VHDR_MODE_IVHDR:
                        return NSIspTuning::EIspProfile_IHDR_Preview;
                }
            }

            return NSIspTuning::EIspProfile_Preview;
        }

        /**
         *@brief In normal shot, you can use this function to get vhdr 3A profile
         * If not vhdr mode, it will return Normal Preview profile.
         *@param[in] vhdrMode : mode in IParamsManager.getVHdr()
         */
        static NSIspTuning::EIspProfile_T getShotVHDR3AProfile(MUINT32 vhdrMode)
        {
            if(vhdrMode == SENSOR_VHDR_MODE_NONE)
                return NSIspTuning::EIspProfile_Preview;

            switch(vhdrMode)
            {
                case SENSOR_VHDR_MODE_ZVHDR:
                    return NSIspTuning::EIspProfile_zHDR_Capture;
                case SENSOR_VHDR_MODE_MVHDR:
                    return NSIspTuning::EIspProfile_MHDR_Capture; // TODO must check available or not
                case SENSOR_VHDR_MODE_IVHDR:
                    return NSIspTuning::EIspProfile_Preview; // TODO currently no support iHDR capture
            }

            return NSIspTuning::EIspProfile_Preview;
        }
        /**
         *@brief In Eng Shot, you can use this function to get vhdr 3A profile.
         * The shot profile must match sensor scenerio for EM mode.
         * If not vhdr mode, it will return Normal Preview profile.
         *@param[in] sensorMode : sensor scenerio ID
         *@param[in] vhdrMode : mode in IParamsManager.getVHdr()
         */
        static NSIspTuning::EIspProfile_T getEngShotVHDR3AProfile(MUINT sensorMode, MUINT32 vhdrMode)
        {
            if(vhdrMode == SENSOR_VHDR_MODE_NONE)
                return NSIspTuning::EIspProfile_Preview;

            if(sensorMode == SENSOR_SCENARIO_ID_NORMAL_PREVIEW)
            {
                switch(vhdrMode)
                {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        return NSIspTuning::EIspProfile_zHDR_Preview;
                    case SENSOR_VHDR_MODE_MVHDR:
                        return NSIspTuning::EIspProfile_MHDR_Preview;
                    case SENSOR_VHDR_MODE_IVHDR:
                        return NSIspTuning::EIspProfile_IHDR_Preview;
                }
            }
            else if(sensorMode == SENSOR_SCENARIO_ID_NORMAL_VIDEO)
            {
                switch(vhdrMode)
                {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        return NSIspTuning::EIspProfile_zHDR_Video;
                    case SENSOR_VHDR_MODE_MVHDR:
                        return NSIspTuning::EIspProfile_MHDR_Video;
                    case SENSOR_VHDR_MODE_IVHDR:
                        return NSIspTuning::EIspProfile_IHDR_Video;
                }
            }
            else if(sensorMode == SENSOR_SCENARIO_ID_NORMAL_CAPTURE)
            {
                switch(vhdrMode)
                {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        return NSIspTuning::EIspProfile_zHDR_Capture;
                    case SENSOR_VHDR_MODE_MVHDR:
                        return NSIspTuning::EIspProfile_MHDR_Capture; // TODO must check available or not
                    case SENSOR_VHDR_MODE_IVHDR:
                        return NSIspTuning::EIspProfile_Preview; // TODO currently no support iHDR capture
                }
            }
            return NSIspTuning::EIspProfile_Preview;
        }


        /**
         *@brief VHdrHelper destructor
         */
        ~VHdrHelper() {};
};


#endif

