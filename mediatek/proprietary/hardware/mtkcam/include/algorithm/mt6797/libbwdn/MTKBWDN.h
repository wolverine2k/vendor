/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */

 /* MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*
**
** Copyright 2008, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#ifndef _MTK_BWDN_H
#define _MTK_BWDN_H

#include "MTKBWDNType.h"
#include "MTKBWDNErrCode.h"
#include "cltk_api.h"

#define BWDN_MAX_SPLIT      64
#ifdef __ANDROID__
    #define __OCL_GPU__
    //#define PC_UTIL
#else
    //#define __OCL_GPU__
    #define PC_UTIL
#endif

typedef enum DRVBWDNObject_s {
    DRV_BWDN_OBJ_NONE = 0,
    DRV_BWDN_OBJ_SW,
    DRV_BWDN_OBJ_UNKNOWN = 0xFF,
} DrvBWDNObject_e;

typedef struct{
    int threads;
    int splitSize;
    int isRotate; 
    float convMat[3];

#ifdef __OCL_GPU__
    /* CLTK context */
    cltk_context ctx;
    /* CLTK image/buffers */
    cltk_image s_Buffer;
    cltk_image us_Buffer1;
    cltk_image us_Buffer2;
    cltk_image us_Buffer3;

    cltk_image f_BufferDown;
    cltk_image f_BufferShad1;
    cltk_image f_BufferShad2;
    cltk_image f_BufferShad3;
    cltk_image f_BufferShad4;
    cltk_image us_Buffer4;
    cltk_image us_Buffer5;
    cltk_image us_Buffer6;

    cltk_image uc_Buffer;
    cltk_image f_Buffer1;
    cltk_image f_Buffer2;
    cltk_image f_Buffer3;
    cltk_image f_BufferTrans;
#else
    unsigned short *us_Buffer1;
    unsigned short *us_Buffer2;
    unsigned short *us_Buffer3;

    unsigned short *us_Buffer4;
    unsigned short *us_Buffer5;
    unsigned short *us_Buffer6;
    unsigned char *uc_Buffer;

    float *f_Buffer1;
    float *f_Buffer2;
    float *f_Buffer3;
#endif
}BWDN_INITPARAMS;

typedef struct {
    //ISP Info
    int OBOffsetBayer[4];
    int OBOffsetMono[4];
    int sensorGainBayer;
    int sensorGainMono;
    int ispGainBayer;
    int ispGainMono;
    int preGainBayer[3];
    int preGainMono[3];
    int isRotate;
    int bayerOrder;
    int RA;

    int OffsetX;
    int OffsetY;

    int BW_SingleRange;
    int BW_OccRange;
    int BW_Range;
    int BW_Kernel;
    int B_Range;
    int B_Kernel;
    int W_Range;
    int W_Kernel;

    int VSL;
    int VOFT;
    int VGAIN;
        
    float Trans[9];
    int dPadding[2];

    int width;
    int height;

    int dwidth; 
    int dheight;
    int dPitch;
    int dsH;
    int dsV;

    int FPREPROC;
    int FSSTEP;

    #ifdef PC_UTIL
    bool grayOnly;
    bool histEqual;
    bool unpack;
    #endif
} BWDN_PARAMS;

typedef struct{
    //unpadded width, height
    int width;
    int height;

    // Input Data
    // (16 + Width + 16) x (16 + Height + 16) 12-bit Mono
    unsigned short* MonoProcessedRaw;
    // (16 + Width + 16) x (16 + Height + 16) 12-bit RGB-Bayer
    unsigned short* BayerProcessedRaw;
    // (16 + Width + 16) x (16 + Height + 16) 12-bit BayerW
    unsigned short* BayerW;                     

    //480x272
    short* depth;

    // Full Shading Gain
    float* BayerGain;
    float* MonoGain;

    // RGB denoise output after BWDN
    unsigned short* output;            // Width x Height x 4 RGB 12-bit image
} BWDNBuffers;

typedef enum
{
    BWDN_FEATURE_BEGIN,              // minimum of feature id
    BWDN_FEATURE_SET_PARAMS,         // feature id to set images
    BWDN_FEATURE_SET_IMAGES,         // feature id to set images
    BWDN_FEATURE_GET_WORKBUF_SIZE,   // feature id to query buffer size
    BWDN_FEATURE_SET_WORKBUF_ADDR,   // feature id to set working buffer address
    BWDN_FEATURE_GET_RESULT,         // feature id to get result
    BWDN_FEATURE_GET_LOG,            // feature id to get debugging information
} BWDN_FEATURE_ENUM;

/*******************************************************************************
*
********************************************************************************/
class MTKBWDN {
public:
    static MTKBWDN* createInstance(DrvBWDNObject_e eobject);
    virtual void   destroyInstance(MTKBWDN* obj) = 0;
       
    virtual ~MTKBWDN(){}
    // Process Control
    virtual MRESULT BWDNInit(void *InitInData, void *InitOutData);    // Env/Cb setting
    virtual MRESULT BWDNMain(void);                                         // START
    virtual MRESULT BWDNReset(void);                                        // RESET

    // Feature Control        
    virtual MRESULT BWDNFeatureCtrl(MUINT32 FeatureID, void* pParaIn, void* pParaOut);
private:
    
};

class AppBWDNTmp : public MTKBWDN {
public:
    //
    static MTKBWDN* getInstance();
    virtual void destroyInstance(MTKBWDN* obj) = 0;
    //
    AppBWDNTmp() {}; 
    virtual ~AppBWDNTmp() {};
};

#endif

