/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "StereoShot"
//
#include "StereoShot.h"
// shot mode enum.
#include <Modes.h>
//
#include <camshot/_callbacks.h>
// support createRawImageStreamInfo and createImageStreamInfo
#include <CamShotUtils.h>
//
#include <IHalSensor.h>
//
#include <camera/MtkCamera.h>
//
#include <metadata/mtk_platform_metadata_tag.h>

#include <hwutils/HwInfoHelper.h>
#include <hwutils/CamManager.h>

#include <LegacyPipeline/ILegacyPipeline.h>
#include <LegacyPipeline/StreamId.h>
#include <LegacyPipeline/buffer/StreamBufferProvider.h>
#include <LegacyPipeline/IResourceContainer.h>
#include <LegacyPipeline/buffer/StreamBufferProviderFactory.h>
#include <LegacyPipeline/LegacyPipelineBuilder.h>
// stereo
#include <LegacyPipeline/stereo/StereoLegacyPipelineBuilder.h>
#include <LegacyPipeline/NodeId.h>
#include <LegacyPipeline/stereo/StereoPipelineData.h>
#include <LegacyPipeline/stereo/StereoLegacyPipelineDef.h>
//
#include <common/vsdof/hal/stereo_size_provider.h>
#include <common/vsdof/hal/stereo_setting_provider.h>
#include <SImager/ISImagerDataTypes.h>
// for debug use
#include <Misc.h>
#include <chrono>
#include <string>
using namespace android;
using namespace NSCam::Utils;
using namespace NSCam::v1;
using namespace NSCam::v1::NSLegacyPipeline;
using namespace NSCamHW;
using namespace NSShot;
using namespace NSCamShot;
using namespace NS3Av3;

#define CHECK_OBJECT(x)  do{                                        \
    if (x == nullptr) { MY_LOGE("Null %s Object", #x); return MFALSE;} \
} while(0)
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define FUNC_START  MY_LOGD("+")
#define FUNC_END    MY_LOGD("-")
#define FUNC_NAME   MY_LOGD("")
//
// in stereo shot, it needs 8 image buffer.
// Raw*3, Bokeh image, clean image, JPS, depth map, and extra data.
#define RESOURCE_CONTAINER_ID 628
/******************************************************************************
 *
 ******************************************************************************/
template <typename T>
inline MBOOL
tryGetMetadata(
    IMetadata* pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if( pMetadata == NULL ) {
        MY_LOGW("pMetadata == NULL");
        return MFALSE;
    }

    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}
/******************************************************************************
 *
 ******************************************************************************/
extern "C"
sp<IShot>
createInstance_StereoShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    FUNC_START;
    sp<IShot>       pShot = NULL;
    sp<StereoShot>  pImpShot = NULL;
    //
    //  (1) new Implementator.
    pImpShot = new StereoShot(pszShotName, u4ShotMode, i4OpenId);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new StereoShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot.clear();
    }
    //
    FUNC_END;
    return  pShot;
}
/******************************************************************************
 *
 ******************************************************************************/
StereoShot::
StereoShot(
    char const*const pszShotName,
    uint32_t const u4ShotMode,
    int32_t const i4OpenId
)
    : ImpShot(pszShotName, u4ShotMode, i4OpenId)
{
    MY_LOGD("ctor(0x%x)", this);
}
/******************************************************************************
 *
 ******************************************************************************/
StereoShot::
~StereoShot()
{
    MY_LOGD("dctor(0x%x)", this);
}
/******************************************************************************
 *
 ******************************************************************************/
void
StereoShot::
cleanDstStream()
{
    char cLogLevel[PROPERTY_VALUE_MAX];
    ::property_get("debug.vsdof.dumpcapturedata", cLogLevel, "0");
    MINT32 value = ::atoi(cLogLevel);
    if(value>0)
    {
        mbEnableDumpCaptureData = MTRUE;
        auto now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
        auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
        msFilename = std::string("/sdcard/vsdof/Cap/")+std::to_string(millis);
        MY_LOGD("filename(%s)", msFilename.c_str());
        NSCam::Utils::makePath(msFilename.c_str(), 0660);
    }
    mvCBList.clear();
    miImgCount = 0;
    mbCBShutter = MTRUE;
}
/******************************************************************************
 *
 ******************************************************************************/
void
StereoShot::
onDestroy()
{
}
/******************************************************************************
 *
 ******************************************************************************/
bool
StereoShot::
sendCommand(
    uint32_t const  cmd,
    MUINTPTR const  arg1,
    uint32_t const  arg2,
    uint32_t const  arg3
)
{
    ATRACE_CALL();
    bool ret = true;
    //
    switch  (cmd)
    {
    //  This command is to reset this class. After captures and then reset,
    //  performing a new capture should work well, no matter whether previous
    //  captures failed or not.
    //
    //  Arguments:
    //          N/A
    /*case eCmd_reset:
        ret = onCmd_reset();
        break;

    //  This command is to perform capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_capture:
        ret = onCmd_capture();
        break;

    //  This command is to perform cancel capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_cancel:
        onCmd_cancel();
        break;
    case eCmd_setDofLevel:
        miBokehLevel = arg2;
	    MY_LOGD("Set bokeh level(%d)", miBokehLevel);
		break;*/
    // temp solution to fix capture does not know af point
    /*case eCmd_setAFRegion:
        maAFRegion = ((MINT32*)arg1);
        MY_LOGD("AF region xmin(%d) ymin(%d) xmax(%d) ymax(%d) weight(%d)",
                maAFRegion[0],
                maAFRegion[1],
                maAFRegion[2],
                maAFRegion[3],
                maAFRegion[4]);
        break;*/
    //
    default:
        ret = ImpShot::sendCommand(cmd, arg1, arg2, arg3);
    }
    //
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
StereoShot::
onCmd_reset()
{
    ATRACE_CALL();
    bool ret = true;
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
StereoShot::
onCmd_capture()
{
    FUNC_START;
    bool ret = true;
    /*ATRACE_CALL();
    bool ret = true;
    if(eShotMode_ZsdShot == mu4ShotMode)
    {
        ZsdCapture();
    }
    else
    {
        MY_LOGE("Unsupported shot mode.");
    }*/
    FUNC_END;
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
/*bool
StereoShot::
ZsdCapture()
{
    FUNC_START;
    bool ret = true;
    ATRACE_CALL();
    //
    Mutex::Autolock _l(mCaptureLock);
    //
    sp<IResourceContainer> pResourceContainer = nullptr;
    //
    IMetadata appMeta = mShotParam.mAppSetting;
    // In stereo capture, hal meta is not get from ShotParam.
    IMetadata halMeta;
    //
    MINT32 main1Id = -1, main2Id = -1;
    if(!StereoSettingProvider::getStereoSensorIndex(main1Id, main2Id))
    {
        MY_LOGE("Get sensor id fail");
        return false;
    }
    MY_LOGD("Main1Id(%d) Main2Id(%d)", main1Id, main2Id);
    //
    const MUINT32 scenario = SENSOR_SCENARIO_ID_NORMAL_CAPTURE;
    mSensorParam = NSCamShot::SensorParam(
            getOpenId(),                             //sensor idx
            scenario,                                // Scenaio
            10,                                      //bit depth
            MFALSE,                                  //bypass delay
            MFALSE);                                 // bypass scenario
    // query buffer provider from ResourceContainer.
    pResourceContainer = IResourceContainer::getInstance(main1Id);
    CHECK_OBJECT(pResourceContainer);
    //
    mpBufferProviderIMGO_Main1 =
                        pResourceContainer->queryConsumer(
                                            eSTREAMID_IMAGE_PIPE_RAW_OPAQUE);
    mpBufferProviderRRZO_Main1 =
                        pResourceContainer->queryConsumer(
                                            eSTREAMID_IMAGE_PIPE_RAW_RESIZER);
    //
    pResourceContainer = IResourceContainer::getInstance(main2Id);
    CHECK_OBJECT(pResourceContainer);
    //
    mpBufferProviderRRZO_Main2 =
                        pResourceContainer->queryConsumer(
                                            eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01);
    CHECK_OBJECT(mpBufferProviderIMGO_Main1);
    CHECK_OBJECT(mpBufferProviderRRZO_Main1);
    CHECK_OBJECT(mpBufferProviderRRZO_Main2);
    //
    if(createManagers(
            mpImageStreamManager,
            mpMetaStreamManager,
            mpNodeConfigManager,
            StereoPipelineMgrData::p2::cap::gStereoMetaTbl_P2Cap,
            StereoPipelineMgrData::p2::cap::gStereoP2ConnectSetting_P2Cap,
            StereoPipelineMgrData::p2::cap::gStereoImgStreamTbl_P2Cap) != OK)
    {
        MY_LOGE("Create manager fail");
        return false;
    }
    // update image stream info to ImageStreamManager
    mpImageStreamManager->updateStreamInfo(
                        eSTREAMID_IMAGE_PIPE_RAW_OPAQUE,
                        mpBufferProviderIMGO_Main1->queryImageStreamInfo());
    mpImageStreamManager->updateStreamInfo(
                        eSTREAMID_IMAGE_PIPE_RAW_RESIZER,
                        mpBufferProviderRRZO_Main1->queryImageStreamInfo());
    mpImageStreamManager->updateStreamInfo(
                        eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01,
                        mpBufferProviderRRZO_Main2->queryImageStreamInfo());
    // get selector
    mpSelector_full = mpBufferProviderIMGO_Main1->querySelector();
    mpSelector_resized = mpBufferProviderRRZO_Main1->querySelector();
    mpSelector_resized_main2 = mpBufferProviderRRZO_Main2->querySelector();
    CHECK_OBJECT(mpSelector_full);
    CHECK_OBJECT(mpSelector_resized);
    CHECK_OBJECT(mpSelector_resized_main2);
    //
    // create stream and pipeline
    //
    if(prepareMetadataAndImageBuffer(&appMeta, &halMeta)!=OK)
    {
        MY_LOGE("Prepare metadata fail.");
        return false;
    }
    //
    if (createPipeline()!=OK) {
        MY_LOGE("createPipeline failed");
        return MFALSE;
    }
    //
    if( OK != mpPipeline->submitSetting(
                0,
                appMeta,
                halMeta
                )
      )
    {
        MY_LOGE("submitRequest failed");
        return MFALSE;
    }
    //
    mpPipeline->waitUntilDrained();
    //
    mpPipeline = nullptr;
    mpCallbackHandler = nullptr;
    mpFactory = nullptr;
    mvCBList.clear();
    //
    MY_LOGD("Ready to return IMGO main1 Buffer(%p)",
                    mpIMGOBuf_Main1->getImageBufferHeap());
    mpSelector_full->returnBuffer(mpIMGOBuf_Main1->getImageBufferHeap());
    //
    MY_LOGD("Ready to return IMGO main1 Buffer(%p)",
                    mpRRZOBuf_Main1->getImageBufferHeap());
    mpSelector_resized->returnBuffer(mpIMGOBuf_Main1->getImageBufferHeap());
    //
    MY_LOGD("Ready to return IMGO main1 Buffer(%p)",
                    mpRRZOBuf_Main2->getImageBufferHeap());
    mpSelector_resized_main2->returnBuffer(mpIMGOBuf_Main1->getImageBufferHeap());
    return true;
}*/
/******************************************************************************
 *
 ******************************************************************************/
void
StereoShot::
onCmd_cancel()
{
    ATRACE_CALL();
}
/******************************************************************************
 *
 ******************************************************************************/
/*MERROR
StereoShot::
createManagers(
    sp<ImageStreamManager>& pImageStreamManager,
    sp<MetaStreamManager>& pMetaStreamManager,
    sp<NodeConfigDataManager>& pNodeConfigManager,
    const MetaStreamManager::metadata_info_setting* metaTable,
    const MINT32* nodeTable,
    ImageStreamManager::image_stream_info_pool_setting* imageTable
)
{
    FUNC_START;

    ATRACE_CALL();
    //
    StereoSizeProvider* pStereoSizeProvider = StereoSizeProvider::getInstance();
    //
    MUINT32 const openId     = mSensorParam.u4OpenID;
    MUINT32 const sensorMode = mSensorParam.u4Scenario;
    MUINT32 const bitDepth   = mSensorParam.u4Bitdepth;
    //
    ImageStreamManager::image_stream_info_pool_setting* imgTbl =
                    StereoPipelineMgrData::p2::cap::gStereoImgStreamTbl_P2Cap;
    //
    MSize const postviewsize =
        MSize(mShotParam.mi4PostviewWidth, mShotParam.mi4PostviewHeight);
    MINT const postviewfmt   = mShotParam.miPostviewDisplayFormat;
    // NOTE: jpeg & thumbnail yuv only support eImgFmt_BLOB
    MSize const jpegsize     = (getRotation() & eTransform_ROT_90) ?
        MSize(mShotParam.mi4PictureHeight, mShotParam.mi4PictureWidth):
        MSize(mShotParam.mi4PictureWidth, mShotParam.mi4PictureHeight);
    MSize const thumbnailsize =
        MSize(mJpegParam.mi4JpegThumbWidth, mJpegParam.mi4JpegThumbHeight);
    // depthmap size
    StereoArea depthMap = pStereoSizeProvider->getBufferSize(
                                    E_DEPTH_MAP,
                                    eSTEREO_SCENARIO_CAPTURE);
    MINT32 depthmap_w = depthMap.size.w;
    MINT32 depthmap_h = depthMap.size.h;
    MSize const depthMapSize = (getRotation() & eTransform_ROT_90) ?
        MSize(depthmap_h, depthmap_w):
        MSize(depthmap_w, depthmap_h);
    // get bokeh image size
    StereoArea vsdofImg = pStereoSizeProvider->getBufferSize(
                                    E_BOKEH_WROT,
                                    eSTEREO_SCENARIO_CAPTURE);
    MINT32 vsdof_w = vsdofImg.size.w;
    MINT32 vsdof_h = vsdofImg.size.h;
    MSize const bokehOutSize = (getRotation() & eTransform_ROT_90) ?
        MSize(vsdof_h, vsdof_w):
        MSize(vsdof_w, vsdof_h);
    // update image table
    {*/
        /******************************************************************************
         *
         ******************************************************************************/
        /*auto updateImgTbl = [](
            ImageStreamManager::image_stream_info_pool_setting* pTable,
            NSCam::v3::StreamId_T id,
            MINT32 imgFmt,
            MSize size
            )->MINT32
        {*/
            /******************************************************************************
            *
            ******************************************************************************/
            /*auto getStreamIndex = [](
                ImageStreamManager::image_stream_info_pool_setting* pTable,
                NSCam::v3::StreamId_T id)
            {
                MINT index = 0;
                while(pTable->streamId != 0)
                {
                    if(pTable->streamId == id)
                    {
                        return index;
                    }
                    pTable++;
                    index++;
                }
                return (-1);
            };
            MINT32 index = getStreamIndex(pTable, id);
            if(index == -1)
            {
                MY_LOGE("Stream do not exist in table!! %d", id);
                return BAD_VALUE;
            }
            pTable[index].imgFormat = imgFmt;
            pTable[index].imgSize = size;
            return MTRUE;
        };
        //
        MBOOL ret = MFALSE;
        ret = updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_JPG_CleanMainImg, eImgFmt_BLOB, jpegsize);
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_JPG_Bokeh, eImgFmt_BLOB, jpegsize);
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_DEPTHMAPNODE_DEPTHMAPYUV, eImgFmt_STA_BYTE, depthMapSize);
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL, eImgFmt_YV12, thumbnailsize);
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_BOKEHNODE_CLEANIMAGEYUV, eImgFmt_NV21, bokehOutSize);
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_BOKEHNODE_RESULTYUV, eImgFmt_NV21, bokehOutSize);
        // ensure JPS size
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_JPG_JPS, eImgFmt_BLOB,
                    pStereoSizeProvider->getSBSImageSize());
        // extra debug
        ret &= updateImgTbl(imgTbl, eSTREAMID_IMAGE_PIPE_STEREO_DBG, eImgFmt_STA_BYTE,
                    MSize(StereoSettingProvider::getExtraDataBufferSizeInBytes(),
                    1));
        if(!ret)
        {
            MY_LOGE("Update tbl fail.");
            return UNKNOWN_ERROR;
        }
    }
    // create image stream manager
    pImageStreamManager =
            ImageStreamManager::create(
                imgTbl
                );
    CHECK_OBJECT(pImageStreamManager);
    // create meta stream manager
    pMetaStreamManager =
            MetaStreamManager::create(metaTable);
    CHECK_OBJECT(pMetaStreamManager);
    // create nodeConfigData manager
    MINT32 openId_main1 = -1, openId_main2 = -1;
    StereoSettingProvider::getStereoSensorIndex(openId_main1, openId_main2);
    const NodeConfigDataManager::NodeConfigMgrSetting cfgSetting =
                                        {
                                            openId_main1,
                                            openId_main2,
                                            nodeTable
                                        };
    pNodeConfigManager =
            NodeConfigDataManager::create("Cap", &cfgSetting);
    CHECK_OBJECT(pNodeConfigManager);
    //
    FUNC_END;
    return OK;
}*/
/******************************************************************************
 *
 ******************************************************************************/
/*MBOOL
StereoShot::
createPipeline()
{
    CHECK_OBJECT(mpIMGOBuf_Main1);
    CHECK_OBJECT(mpRRZOBuf_Main1);
    CHECK_OBJECT(mpRRZOBuf_Main2);
    //
    sp<StereoLegacyPipelineBuilder> pBuilder =
            new StereoLegacyPipelineBuilder(
                                        getOpenId(),
                                        "P2_Pipeline_Cap",
                                        StereoLegacyPipelineMode_T::STPipelineMode_P2);
    CHECK_OBJECT(pBuilder);
    //
    sp<IResourceContainer> pResourceContainierCap = IResourceContainer::getInstance(RESOURCE_CONTAINER_ID);
    CHECK_OBJECT(pResourceContainierCap);
    //
    mpCallbackHandler = new BufferCallbackHandler(mSensorParam.u4OpenID);
    mpCallbackHandler->setImageCallback(this);
    //
    mpFactory =
                StreamBufferProviderFactory::createInstance();
    //
    MY_LOGD("create pipeline for zsd");
    // set buffer
    {
        auto setBufferProvider=[this](
                            StreamId_T id,
                            sp<IImageBuffer> pBuf)->MBOOL
        {
            sp<IImageStreamInfo> pTempStreamInfo = nullptr;
            sp<CallbackBufferPool> pPool = nullptr;
            pTempStreamInfo =
                    mpImageStreamManager->getStreamInfoByStreamId(id);
            if(pTempStreamInfo == nullptr)
            {
                MY_LOGE("update streamId(%d) fail.", id);
                return MFALSE;
            }
            pPool = new CallbackBufferPool(pTempStreamInfo);
            if(pBuf != nullptr)
            {
                pPool->addBuffer(pBuf);
            }
            else
            {
                pPool->allocateBuffer(
                        pTempStreamInfo->getStreamName(),
                        pTempStreamInfo->getMaxBufNum(),
                        pTempStreamInfo->getMinInitBufNum());
            }
            mpCallbackHandler->setBufferPool(pPool);
            mpFactory->setImageStreamInfo(pTempStreamInfo);
            mpFactory->setUsersPool(
                            mpCallbackHandler->queryBufferPool(pTempStreamInfo->getStreamId())
                            );
            mpImageStreamManager->updateBufProvider(
                                            id,
                                            mpFactory->create(RESOURCE_CONTAINER_ID, MTRUE),
                                            Vector<StreamId_T>());
            //
            mvCBList.push_back(id);
            return MTRUE;
        };
        MBOOL setBufferRet = MFALSE;
        mvCBList.clear();
        setBufferRet = setBufferProvider(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE, mpIMGOBuf_Main1);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_RAW_RESIZER, mpRRZOBuf_Main1);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01, mpRRZOBuf_Main2);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_JPG_CleanMainImg, nullptr);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_JPG_Bokeh, nullptr);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_JPG_JPS, nullptr);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_DEPTHMAPNODE_DEPTHMAPYUV, nullptr);
        setBufferRet &= setBufferProvider(eSTREAMID_IMAGE_PIPE_STEREO_DBG, nullptr);
        if(!setBufferRet)
        {
            MY_LOGE("set buffer provider fail.");
            return setBufferRet;
        }
        MY_LOGD("needs to wait (%d) callbacks", mvCBList.size());
    }
    //
    pBuilder->setMetaStreamId(
                eSTREAMID_META_HAL_DYNAMIC_P1,
                eSTREAMID_META_APP_CONTROL);
    //
    mpPipeline = pBuilder->create(
                        mpMetaStreamManager,
                        mpImageStreamManager,
                        mpNodeConfigManager);
    CHECK_OBJECT(mpPipeline);
    {
        sp<ResultProcessor> pResultProcessor = mpPipeline->getResultProcessor().promote();
        CHECK_OBJECT(pResultProcessor);
        //partial
        pResultProcessor->registerListener( 0, 0, true, this);
        //full
        pResultProcessor->registerListener( 0, 0, false, this);
        //
        {
            MINT64 timestamp_P1;
            if(tryGetMetadata<MINT64>(&mSelectorAppMetadata_main1, MTK_SENSOR_TIMESTAMP, timestamp_P1)){
                MY_LOGD("timestamp_P1:%lld", timestamp_P1);
            }else{
                MY_LOGW("Can't get timestamp from mSelectorAppMetadata_main1, set to 0");
                timestamp_P1 = 0;
            }
            {
                auto doTimestampManually = [](
                                            sp<IResourceContainer> pResourceContainierCap,
                                            StreamId_T id,
                                            MINT64 timestamp)->MBOOL
                {
                    sp<StreamBufferProvider> provider =
                                        pResourceContainierCap->queryConsumer(id);
                    if(provider==nullptr)
                    {
                        MY_LOGE("get provider fail. streamId(0x%x)", id);
                        return MFALSE;
                    }
                    provider->doTimestampCallback(0, MFALSE, timestamp);
                    return MTRUE;
                };
                MBOOL timestamp_ret = MFALSE;
                timestamp_ret = doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_RAW_RESIZER, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_RAW_OPAQUE, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_JPG_CleanMainImg, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_JPG_Bokeh, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_JPG_JPS, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_DEPTHMAPNODE_DEPTHMAPYUV, timestamp_P1);
                timestamp_ret &= doTimestampManually(pResourceContainierCap, eSTREAMID_IMAGE_PIPE_STEREO_DBG, timestamp_P1);
                if(!timestamp_ret)
                {
                    MY_LOGE("doTimestampManually faile.");
                    return MFALSE;
                }
            }
        }
        mAppDone = MFALSE;
        mHalDone = MFALSE;

        mAppResultMetadata.clear();
        mHalResultMetadata.clear();
    }
    return OK;
}*/
/******************************************************************************
 *
 ******************************************************************************/
/*MERROR
StereoShot::
prepareMetadataAndImageBuffer(
    IMetadata* appSetting,
    IMetadata* halSetting)
{
    FUNC_START;

    android::sp<IImageBufferHeap> spHeap_full           = nullptr;
    android::sp<IImageBufferHeap> spHeap_resized        = nullptr;
    android::sp<IImageBufferHeap> spHeap_resized_main2  = nullptr;
    Vector<ISelector::MetaItemSet> tempMetadata;
    IMetadata appDynamic;

    // step 1
    // getResult from main1_resized
    // appMeta and halMeta also updated by this Selector
    {
        MY_LOGD("getResult from main1_resized  +");
        tempMetadata.clear();

        MINT32 tempReqNo = -1;
        if(mpSelector_resized->getResult(tempReqNo, tempMetadata, spHeap_resized) != OK){
            MY_LOGE("mSelector_resized->getResult failed!");
        }

        MBOOL index_appDynamic = MFALSE;
        MBOOL index_halDynamic = MFALSE;
        for ( auto metadata : tempMetadata ){
            switch(metadata.id){
                case eSTREAMID_META_APP_DYNAMIC_P1:
                    mSelectorAppMetadata_main1 = appDynamic = metadata.meta;
                    index_appDynamic = MTRUE;
                break;
                case eSTREAMID_META_HAL_DYNAMIC_P1:
                    *halSetting = metadata.meta;
                    index_halDynamic = MTRUE;
                break;
                default:
                    MY_LOGE("unexpected meta stream from selector:%d", metadata.id);
                break;
            }
        }
        if(!index_appDynamic || !index_halDynamic)
        {
            MY_LOGE("some meta stream from P1_main1 is not correctly set!"
            );
            return UNKNOWN_ERROR;
        }

        MY_LOGD("getResult from main1_resized  -");
    }

    // step 2
    // getResult from main1_full
    {
        MY_LOGD("getResult from main1_full  +");
        tempMetadata.clear();

        MINT32 tempReqNo = -1;
        if(mpSelector_full->getResult(tempReqNo, tempMetadata, spHeap_full) != OK){
            MY_LOGE("mSelector_full->getResult failed!");
        }
        MY_LOGD("getResult from main1_full  -");
    }

    // step 3
    // getResult from main2_resized
    {
        MY_LOGD("getResult from main2_resized  +");
        tempMetadata.clear();

        MINT32 tempReqNo = -1;
        if(mpSelector_resized_main2->getResult(tempReqNo, tempMetadata, spHeap_resized_main2) != OK){
            MY_LOGE("mSelector_resized_main2->getResult failed!");
        }

        MBOOL index_appDynamic = MFALSE;
        MBOOL index_halDynamic = MFALSE;
        IMetadata::IEntry entry_meta(MTK_P1NODE_MAIN2_HAL_META);
        for ( auto metadata : tempMetadata ){
            switch(metadata.id){
                case eSTREAMID_META_APP_DYNAMIC_P1_MAIN2:
                    index_appDynamic = MTRUE;
                    break;
                case eSTREAMID_META_HAL_DYNAMIC_P1_MAIN2:
                    MY_LOGD("push main2 hal dynamic into hal_meta");
                    entry_meta.push_back(metadata.meta, Type2Type< IMetadata >());
                    halSetting->update(entry_meta.tag(), entry_meta);
                    index_halDynamic = MTRUE;
                    break;
                default:
                    MY_LOGE("unexpected meta stream from selector:%d", metadata.id);
                    break;
            }
        }
        if(!index_appDynamic|| !index_halDynamic)
        {
            MY_LOGE("some meta stream from P1_main2 is not correctly set! (%d ,%d )",
                index_appDynamic,
                index_halDynamic
            );
            return UNKNOWN_ERROR;
        }
        MY_LOGD("getResult from main2_resized  -");
    }

    // step 4
    // get DOF level and push into appSetting
    // temp hard code dof level here
    // will get this value from app
    {
        MY_LOGD("set DOF level +");
        IMetadata::IEntry entry(MTK_STEREO_FEATURE_DOF_LEVEL);
        entry.push_back(miBokehLevel, Type2Type< MINT32 >());
        appSetting->update(entry.tag(), entry);
        MY_LOGD("set DOF level -");
    }
    {
        // force using hw to encode all jpeg
        IMetadata::IEntry entry(MTK_JPG_ENCODE_TYPE);
        entry.push_back(NSCam::NSIoPipe::NSSImager::JPEGENC_HW_ONLY, Type2Type< MUINT8 >());
        halSetting->update(entry.tag(), entry);
    }*/
    // temp solution for af region.
    /*{
        if(maAFRegion[4] == 0)
        {
            IMetadata::IEntry entry(MTK_CONTROL_AF_TRIGGER);
            entry.push_back(0, Type2Type< MUINT8 >());
            appSetting->update(entry.tag(), entry);
        }
        else
        {
            {
                IMetadata::IEntry entry(MTK_CONTROL_AF_TRIGGER);
                entry.push_back(MTK_CONTROL_AF_TRIGGER_START, Type2Type< MUINT8 >());
                appSetting->update(entry.tag(), entry);
            }
            //
            {
                IMetadata::IEntry entry(MTK_CONTROL_AF_REGIONS);
                entry.push_back( maAFRegion[0], Type2Type< MINT32 >());
                entry.push_back( maAFRegion[1], Type2Type< MINT32 >());
                entry.push_back( maAFRegion[2], Type2Type< MINT32 >());
                entry.push_back( maAFRegion[3], Type2Type< MINT32 >());
                entry.push_back( maAFRegion[4], Type2Type< MINT32 >());
                appSetting->update(entry.tag(), entry);
            }
        }
    }*/
	// get image buffer
	/*{
		//
		mpIMGOBuf_Main1 = spHeap_full->createImageBuffer();
		mpRRZOBuf_Main1 = spHeap_resized->createImageBuffer();
		mpRRZOBuf_Main2 = spHeap_resized_main2->createImageBuffer();
        CHECK_OBJECT(mpIMGOBuf_Main1);
        CHECK_OBJECT(mpRRZOBuf_Main1);
        CHECK_OBJECT(mpRRZOBuf_Main2);
	}

#if 1
    // Debug Log
    {
        MSize tempSize;
        if( ! tryGetMetadata<MSize>(const_cast<IMetadata*>(halSetting), MTK_HAL_REQUEST_SENSOR_SIZE, tempSize) ){
            MY_LOGE("cannot get MTK_HAL_REQUEST_SENSOR_SIZE after updating request");
        }else{
            MY_LOGD("MTK_HAL_REQUEST_SENSOR_SIZE:(%dx%d)", tempSize.w, tempSize.h);
        }
        IMetadata tempMetadata;
        if( ! tryGetMetadata<IMetadata>(const_cast<IMetadata*>(halSetting), MTK_P1NODE_MAIN2_HAL_META, tempMetadata) ){
            MY_LOGE("cannot get MTK_P1NODE_MAIN2_HAL_META after updating request");
        }else{
            MY_LOGD("MTK_P1NODE_MAIN2_HAL_META");
        }
        MINT32 tempLevel;
        if( ! tryGetMetadata<MINT32>(const_cast<IMetadata*>(appSetting), MTK_STEREO_FEATURE_DOF_LEVEL, tempLevel) ){
            MY_LOGE("cannot get MTK_STEREO_FEATURE_DOF_LEVEL after updating request");
        }else{
            MY_LOGD("MTK_STEREO_FEATURE_DOF_LEVEL:%d", tempLevel);
        }
        MINT32 tempTransform;
        if( ! tryGetMetadata<MINT32>(const_cast<IMetadata*>(appSetting), MTK_JPEG_ORIENTATION, tempTransform) ){
            MY_LOGE("cannot get MTK_JPEG_ORIENTATION after updating request");
        }else{
            MY_LOGD("MTK_JPEG_ORIENTATION:%d", tempTransform);
        }
    }
#endif
    FUNC_END;
    return OK;
}*/
/******************************************************************************
 * metadata event handler
 ******************************************************************************/
void
StereoShot::
onResultReceived(
    MUINT32    const requestNo,
    StreamId_T const streamId,
    MBOOL      const errorResult,
    IMetadata  const result)
{
    CAM_TRACE_FMT_BEGIN("onMetaReceived No%d,StreamID  %#" PRIxPTR, requestNo,streamId);
    MY_LOGD("requestNo %d, stream %#" PRIxPTR, requestNo, streamId);

    Mutex::Autolock _l(mMetadataLock);

    if (streamId == eSTREAMID_META_APP_FULL)
    {
        mAppDone = MTRUE;
        if(mbSupportDng)
        {
            ssize_t index = mvAppMetadataSet.indexOfKey(requestNo);
            if(index >= 0)
            {
                IMetadata resultAppMetadata = result;
                resultAppMetadata += mvAppMetadataSet.valueAt(index);
                mpShotCallback->onCB_DNGMetaData((MUINTPTR)&resultAppMetadata);
            }
            else
            {
                MY_LOGE("cannot find app metadata in capture Dng mode requestNo(%d)", requestNo);
            }
        }
    }
    else if (streamId == eSTREAMID_META_HAL_FULL)
    {
        mHalDone = MTRUE;
    }
    {
        if (mbCBShutter && streamId == eSTREAMID_META_APP_DYNAMIC_DEPTH)
        {
            mbCBShutter = MFALSE;
            mpShotCallback->onCB_Shutter(true, 0);
        }
    }
    CAM_TRACE_FMT_END();
}
/******************************************************************************
 * image event handler
 ******************************************************************************/
MERROR
StereoShot::
onResultReceived(
    MUINT32    const requestNo,
    StreamId_T const streamId,
    MBOOL      const errorBuffer,
    android::sp<IImageBuffer>& pBuffer
)
{
    ATRACE_CALL();
    Mutex::Autolock _l(mImgResultLock);
    MY_LOGD("image request(%d) streamID(%#x)", requestNo, streamId);
    CHECK_OBJECT(pBuffer);
    if (errorBuffer)
    {
        MY_LOGE("the content of buffer may not correct...");
    }
    pBuffer->lockBuf(LOG_TAG, eBUFFER_USAGE_SW_READ_MASK);

    uint8_t const* puBuf = (uint8_t const*)pBuffer->getBufVA(0);
    MUINT32 u4Size = pBuffer->getBitstreamSize();
    bool isLastFrame = false;
    if(eSTREAMID_IMAGE_PIPE_RAW16 != streamId)
        miImgCount++;
    if(mvCBList.size() == miImgCount)
    {
        isLastFrame = true;
    }
    MY_LOGD("miImgCount(%d) mvCBList(%d)", miImgCount, mvCBList.size());
    MSize size = pBuffer->getImgSize();
    string streamName = "";
    string fileEXT = "";
    string size_x = "";
    string size_y = "";
    const char* extra_data;
    switch (streamId)
    {
        // todo: needs to cb JPS, clean image and depth map.
        case eSTREAMID_IMAGE_PIPE_JPG_Bokeh:    // bokeh result
            streamName = "JPG_BOKEH";
            fileEXT = ".jpg";
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 0, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_COMPRESSED_IMAGE);
            break;
        case eSTREAMID_IMAGE_PIPE_JPG_JPS:      // jps
            streamName = "JPS";
            fileEXT = ".jpg";
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 1, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_JPS);
            // todo: get dbg info from stereo hal
            break;
        case eSTREAMID_IMAGE_PIPE_DEPTHMAPNODE_DEPTHMAPYUV: // depthmap
            streamName = "DEPTHMAP_";
            size_x = std::to_string(size.w)+std::string("_");
            size_y = std::to_string(size.h);
            fileEXT = ".yuv";
            // for byte format, we need to compute size manually.
            u4Size = pBuffer->getBufSizeInBytes(0);
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 2, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_DEPTHMAP);
            break;
        case eSTREAMID_IMAGE_PIPE_JPG_CleanMainImg:
            streamName = "CLEAN_IMG";
            fileEXT = ".jpg";
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 3, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_STEREO_CLEAR_IMAGE);
            break;
        case eSTREAMID_IMAGE_PIPE_STEREO_DBG:
            streamName = "EXTRA_DBG";
            fileEXT = ".dbg";
            // for byte format, we need to compute size manually.
            extra_data = (const char*)puBuf;
            u4Size = strlen(extra_data)+1;
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 4, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_STEREO_DBG);
            break;
        case eSTREAMID_IMAGE_PIPE_STEREO_DBG_LDC:
            streamName = "ldc";
            fileEXT = ".dbg";
            size_x = std::to_string(size.w)+std::string("_");
            size_y = std::to_string(size.h);
            // for byte format, we need to compute size manually.
            u4Size = pBuffer->getBufSizeInBytes(0);
            mpShotCallback->onCB_CompressedImage_packed(0, u4Size, puBuf, 5, isLastFrame, MTK_CAMERA_MSG_EXT_DATA_STEREO_LDC);
            break;
        case eSTREAMID_IMAGE_PIPE_RAW_RESIZER:
            streamName = "RES_";
            size_x = std::to_string(size.w)+std::string("_");
            size_y = std::to_string(size.h);
            fileEXT = ".raw";
            break;
        case eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01:
            streamName = "RES_01_";
            size_x = std::to_string(size.w)+std::string("_");
            size_y = std::to_string(size.h);
            fileEXT = ".raw";
            break;
        case eSTREAMID_IMAGE_PIPE_RAW_OPAQUE:
            streamName = "OPA_";
            size_x = std::to_string(size.w)+std::string("_");
            size_y = std::to_string(size.h);
            fileEXT = ".raw";
            break;
        case eSTREAMID_IMAGE_PIPE_RAW16:
            streamName = "raw16";
            fileEXT = ".dbg";
            mpShotCallback->onCB_Raw16Image(pBuffer.get());
            break;
        default:
            MY_LOGW("not supported streamID(%#x)", streamId);
    }
    // dump data
    if(mbEnableDumpCaptureData)
    {
        MSize size = pBuffer->getImgSize();
        std::string saveFileName = msFilename +
                              std::string("/")+
                              streamName+
                              size_x+
                              size_y+
                              fileEXT;
        pBuffer->saveToFile(saveFileName.c_str());
    }
    pBuffer->unlockBuf(LOG_TAG);

    return OK;
}

/******************************************************************************
*
 ******************************************************************************/
/*MUINT32
StereoShot::
getRotation() const
{
    return mShotParam.mu4Transform;
}*/
/******************************************************************************
 *
 ******************************************************************************/
String8
StereoShot::
getUserName()
{
    ATRACE_CALL();
    return String8(LOG_TAG);
}