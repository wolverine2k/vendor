#define LOG_TAG "DumpTunnel"

#define MTK_LOG_ENABLE 1
#include <dlfcn.h>
#include <utils/String8.h>
#include <binder/Parcel.h>
#include <cutils/log.h>
#include <mediatek/IDumpTunnel.h>

namespace android {

// client : proxy GuiEx class
class BpDumpTunnel : public BpInterface<IDumpTunnel> {
public:
    BpDumpTunnel(const sp<IBinder>& impl)
        :   BpInterface<IDumpTunnel>(impl) {
    }

    virtual ~BpDumpTunnel();

    virtual status_t kickDump(String8& result, const char* prefix) {
        Parcel data, reply;
        data.writeInterfaceToken(IDumpTunnel::getInterfaceDescriptor());
        data.writeString8(result);
        data.writeCString(prefix);
        status_t err = remote()->transact(DUMPTUNNEL_DUMP, data, &reply);
        if (err != NO_ERROR) {
            ALOGE("kickDump could not contact remote\n");
            return err;
        }
        result = reply.readString8();
        err = reply.readInt32();
        return err;
    }
};

// Out-of-line virtual method definition to trigger vtable emission in this
// translation unit (see clang warning -Wweak-vtables)
BpDumpTunnel::~BpDumpTunnel() {}

IMPLEMENT_META_INTERFACE(DumpTunnel, "DumpTunnel");

status_t BnDumpTunnel::onTransact(uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags) {
    switch (code) {
        case DUMPTUNNEL_DUMP: {
            CHECK_INTERFACE(IDumpTunnel, data, reply);
            String8 result;
            const char* prefix = NULL;
            result = data.readString8();
            prefix = data.readCString();

            status_t ret = kickDump(result, prefix);
            reply->writeString8(result);
            reply->writeInt32(ret);
            return NO_ERROR;
        }
    }
    return BBinder::onTransact(code, data, reply, flags);
}

};
